#ifndef __GLOBAL_H__
#define __GLOBAL_H__

const int MAXOBJECTS = 50;

struct SimpleColor {
	float r, g, b;
};

inline SimpleColor operator * (const SimpleColor &col, float c) {
	SimpleColor col2;
	col2.r = col.r * c;
	col2.g = col.g * c;
	col2.b = col.b * c;
	return col2;
}

inline SimpleColor operator + (const SimpleColor &col1, const SimpleColor &col2) {
	SimpleColor col3;
	col3.r = col1.r + col2.r;
	col3.g = col1.g + col2.g;
	col3.b = col1.b + col2.b;
	return col3;
}

struct ImageColor {
	unsigned char r, g, b, a;
};

struct SpaceCoordinate {
	float x, y, z;
};

struct TextureCoordinate {
	float u, v;
};

struct Vector3f {
	float x, y, z;

	Vector3f& operator += (const Vector3f &v2) {
		this->x += v2.x;
		this->y += v2.y;
		this->z += v2.z;
	}
};

struct Phong {
	SimpleColor diffuse;
	SimpleColor specular;
	float shininess;
};

struct Sphere {
	int material;
	cimg_library::CImg<short> * texture;
	SpaceCoordinate position;
	Phong color;
	float refraction;
	float radius;
};

struct Vertex {
	SpaceCoordinate position;
	Vector3f normal;
	Phong color;
	float refraction;
	TextureCoordinate texture_coord;
};

struct Triangle {
	int material;
	cimg_library::CImg<short> * texture;
	Vertex vertices[3];
};

struct AmbientLight {
	SimpleColor color;
};

struct PointLight {
	SpaceCoordinate position;
	SimpleColor color;
};

struct AreaLight {
	SpaceCoordinate vertices[4];
	SimpleColor color;
};

struct Ray {
	SpaceCoordinate origin;
	Vector3f direction;
};

inline SpaceCoordinate operator + (const SpaceCoordinate &p, const Vector3f &v) {
	SpaceCoordinate p2;
	p2.x = p.x + v.x;
	p2.y = p.y + v.y;
	p2.z = p.z + v.z;
	return p2;
}

inline SpaceCoordinate operator - (const SpaceCoordinate &p, const Vector3f &v) {
	SpaceCoordinate p2;
	p2.x = p.x - v.x;
	p2.y = p.y - v.y;
	p2.z = p.z - v.z;
	return p2;
}

inline Vector3f operator + (const Vector3f &v1, const Vector3f &v2) {
	Vector3f v;
	v.x = v1.x + v2.x;
	v.y = v1.y + v2.y;
	v.z = v1.z + v2.z;
	return v;
}

inline Vector3f operator - (const SpaceCoordinate &p1, const SpaceCoordinate &p2) {
	Vector3f v;
	v.x = p1.x - p2.x;
	v.y = p1.y - p2.y;
	v.z = p1.z - p2.z;
	return v;
}

inline Vector3f operator * (float c, const Vector3f &v) {
	Vector3f v1;
	v1.x = v.x * c;
	v1.y = v.y * c;
	v1.z = v.z * c;
	return v1;
}

inline Vector3f operator - (const Vector3f &v1, const Vector3f &v2) {
	Vector3f v;
	v.x = v1.x - v2.x;
	v.y = v1.y - v2.y;
	v.z = v1.z - v2.z;
	return v;
}

inline float operator * (const Vector3f &v1, const Vector3f &v2) {
	return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
}

#endif