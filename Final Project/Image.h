#ifndef __IMAGE_H__
#define __IMAGE_H__

class TGAImage {
public:
	TGAImage();
	TGAImage(int, int);
	void setAllPixels(ImageColor *);
	void setPixel(ImageColor, int, int);
	void writeImage(string);
	void setWidth(int width);
	void setHeight(int height);
	int getWidth();
	int getHeight();
private:
	ImageColor * pixels;
	int width, height;
	int convert2Dto1D(int, int);
};

#endif