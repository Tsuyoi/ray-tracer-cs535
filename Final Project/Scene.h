#ifndef __SCENE_H__
#define __SCENE_H__

class Scene {
public:
	Scene();
	~Scene();
	bool LoadScene(char *);
	Sphere * GetSphere(int);
	int GetSphereCount();
	Triangle * GetTriangle(int);
	int GetTriangleCount();
	PointLight * GetPointLight(int);
	int GetPointLightCount();
	vector<AreaLight> GetAreaLights();
	AmbientLight GetAmbientLight();
	void PrintSpheres();
	void PrintTriangles();
	void PrintAmbientLight();
	void PrintPointLights();
	void PrintAreaLights();
private:
	Sphere * sph[MAXOBJECTS];
	int sphereCount;
	Triangle * tri[MAXOBJECTS];
	int triangleCount;
	PointLight * pl[MAXOBJECTS];
	int pointLightCount;
	AmbientLight ambientLight;
	vector<PointLight> pointLights;
	vector<AreaLight> areaLights;
};

#endif