#include "includes.h"

Scene::Scene() {
	areaLights.clear();
	sphereCount = 0;
	triangleCount = 0;
	pointLightCount = 0;
}

Scene::~Scene() {
}

Sphere * Scene::GetSphere(int index) {
	return sph[index];
}

int Scene::GetSphereCount() {
	return sphereCount;
}

Triangle * Scene::GetTriangle(int index) {
	return tri[index];
}

int Scene::GetTriangleCount() {
	return triangleCount;
}

PointLight * Scene::GetPointLight(int index) {
	return pl[index];
}

int Scene::GetPointLightCount() {
	return pointLightCount;
}

bool Scene::LoadScene(char * fileName) {
	ifstream inFile;
	inFile.open(fileName, ios::in);
	if (!inFile.is_open())
		return false;
	string buffer;
	float fBuffer;
	char * char_buffer = new char[256];
	inFile.getline(char_buffer, 256);
	while (char_buffer[0] == '#')
		inFile.getline(char_buffer, 256);
	int objectCount = atoi(char_buffer);
	inFile >> buffer;
	inFile >> ambientLight.color.r >> ambientLight.color.g >> ambientLight.color.b >> buffer;
	while (!inFile.eof()) {
		if (buffer[0] == '#') {
			inFile.getline(char_buffer, 256);
			inFile >> buffer;
		}
		if (buffer.compare("sphere") == 0) {
			sph[sphereCount] = new Sphere();
			sph[sphereCount]->radius = 0.0;
			sph[sphereCount]->refraction = 0.0;
			for (int j = 0; j < 6; j++) {
				inFile >> buffer;
				buffer[0] = tolower(buffer[0]);
				if (buffer.compare("mt:") == 0) {
					inFile >> buffer;
					buffer[0] = tolower(buffer[0]);
					if (buffer.compare("phong") == 0) {
						sph[sphereCount]->material = 0;
					}
					else if (buffer.compare("glass") == 0) {
						sph[sphereCount]->material = 1;
					}
					else {
						sph[sphereCount]->material = 2;
					}
				} else if (buffer.compare("tex:") == 0) {
					inFile >> buffer;
					if (buffer.compare("null") != 0) {
						sph[sphereCount]->texture = new cimg_library::CImg<short>(buffer.c_str());
					}
				} else if (buffer.compare("pos:") == 0) {
					inFile >> fBuffer;
					sph[sphereCount]->position.x = fBuffer;
					inFile >> fBuffer;
					sph[sphereCount]->position.y = fBuffer; 
					inFile >> fBuffer;
					sph[sphereCount]->position.z = fBuffer;
				} else if (buffer.compare("phong:") == 0) {
					inFile >> fBuffer;
					sph[sphereCount]->color.diffuse.r = fBuffer;
					inFile >> fBuffer;
					sph[sphereCount]->color.diffuse.g = fBuffer;
					inFile >> fBuffer;
					sph[sphereCount]->color.diffuse.b = fBuffer;
					inFile >> fBuffer;
					sph[sphereCount]->color.specular.r = fBuffer;
					inFile >> fBuffer;
					sph[sphereCount]->color.specular.g = fBuffer;
					inFile >> fBuffer;
					sph[sphereCount]->color.specular.b = fBuffer;
					inFile >> fBuffer;
					sph[sphereCount]->color.shininess = fBuffer;
				} else if (buffer.compare("ref:") == 0) {
					inFile >> fBuffer;
					sph[sphereCount]->refraction = fBuffer;
				} else if (buffer.compare("rad:") == 0) {
					inFile >> fBuffer;
					sph[sphereCount]->radius = fBuffer;
				} else {
					cout << "Missing some requirements!" << endl;
				}
			}
			sphereCount++;
		}
		if (buffer.compare("triangle") == 0) {
			tri[triangleCount] = new Triangle();
			inFile >> buffer;
			if (buffer.compare("mt:") == 0) {
				inFile >> buffer;
				buffer[0] = tolower(buffer[0]);
				if (buffer.compare("phong") == 0) {
					tri[triangleCount]->material = 0;
				}
				else if (buffer.compare("glass") == 0) {
					tri[triangleCount]->material = 1;
				}
				else {
					tri[triangleCount]->material = 2;
				}
			}
			inFile >> buffer;
			if (buffer.compare("tex:") == 0) {
				inFile >> buffer;
				if (buffer.compare("null") != 0) {
					tri[triangleCount]->texture = new cimg_library::CImg<short>(buffer.c_str());
				}
			}
			for (int i = 0; i < 3; i++) {
				for (int j = 0; j < 5; j++) {
					inFile >> buffer;
					buffer[0] = tolower(buffer[0]);
					if (buffer.compare("pos:") == 0) {
						inFile >> fBuffer;
						tri[triangleCount]->vertices[i].position.x = fBuffer;
						inFile >> fBuffer;
						tri[triangleCount]->vertices[i].position.y = fBuffer;
						inFile >> fBuffer;
						tri[triangleCount]->vertices[i].position.z = fBuffer;
					} else if (buffer.compare("nor:") == 0) {
						inFile >> fBuffer;
						tri[triangleCount]->vertices[i].normal.x = fBuffer;
						inFile >> fBuffer;
						tri[triangleCount]->vertices[i].normal.y = fBuffer;
						inFile >> fBuffer;
						tri[triangleCount]->vertices[i].normal.z = fBuffer;
					} else if (buffer.compare("phong:") == 0) {
						inFile >> fBuffer;
						tri[triangleCount]->vertices[i].color.diffuse.r = fBuffer;
						inFile >> fBuffer;
						tri[triangleCount]->vertices[i].color.diffuse.g = fBuffer;
						inFile >> fBuffer;
						tri[triangleCount]->vertices[i].color.diffuse.b = fBuffer;
						inFile >> fBuffer;
						tri[triangleCount]->vertices[i].color.specular.r = fBuffer;
						inFile >> fBuffer;
						tri[triangleCount]->vertices[i].color.specular.g = fBuffer;
						inFile >> fBuffer;
						tri[triangleCount]->vertices[i].color.specular.b = fBuffer;
						inFile >> fBuffer;
						tri[triangleCount]->vertices[i].color.shininess = fBuffer;
					} else if (buffer.compare("ref:") == 0) {
						inFile >> fBuffer;
						tri[triangleCount]->vertices[i].refraction = fBuffer;
					} else if (buffer.compare("uv:") == 0) {
						inFile >> fBuffer;
						tri[triangleCount]->vertices[i].texture_coord.u = fBuffer;
						inFile >> fBuffer;
						tri[triangleCount]->vertices[i].texture_coord.v = fBuffer;
					} else {
						cout << "Missing Information!" << endl;
					}
				}
			}
			triangleCount++;
		}
		if (buffer.compare("point-light") == 0) {
			pl[pointLightCount] = new PointLight();
			inFile >> buffer;
			if (buffer.compare("pos:") == 0) {
				inFile >> fBuffer;
				pl[pointLightCount]->position.x = fBuffer;
				inFile >> fBuffer;
				pl[pointLightCount]->position.y = fBuffer;
				inFile >> fBuffer;
				pl[pointLightCount]->position.z = fBuffer;
			}
			inFile >> buffer;
			if (buffer.compare("col:") == 0) {
				inFile >> fBuffer;
				pl[pointLightCount]->color.r = fBuffer;
				inFile >> fBuffer;
				pl[pointLightCount]->color.g = fBuffer;
				inFile >> fBuffer;
				pl[pointLightCount]->color.b = fBuffer;
			}
			pointLightCount++;
		}
		if (buffer.compare("area-light") == 0) {
			AreaLight temp;
			for (int i = 0; i < 4; i++) {
				inFile >> buffer;
				if (buffer.compare("pos:") == 0) {
					inFile >> temp.vertices[i].x >> temp.vertices[i].y >> temp.vertices[i].z;
				}
			}
			inFile >> buffer;
			if (buffer.compare("col:") == 0) {
				inFile >> temp.color.r >> temp.color.g >> temp.color.b;
			}
			areaLights.push_back(temp);
		}
		inFile >> buffer;
	}
	inFile.close();
	return true;
}

vector<AreaLight> Scene::GetAreaLights() {
	return areaLights;
}

AmbientLight Scene::GetAmbientLight() {
	return ambientLight;
}

void Scene::PrintAmbientLight() {
	cout << "Ambient Light" << endl;
	cout << " - Color: {" << ambientLight.color.r << ", " << ambientLight.color.g << ", " << ambientLight.color.b << "}" << endl;
}

void Scene::PrintAreaLights() {
	for (int i = 0; i < (int)areaLights.size(); i++) {
		cout << "Area Light #" << i << endl;
		cout << " - Color: {" << areaLights[i].color.r << ", " << areaLights[i].color.g << ", " << areaLights[i].color.b << "}" << endl;
		cout << " - Vertices" << endl;
		for (int j = 0; j < 4; j++) {
			cout << "   - Vertex #" << j << endl;
			cout << "     - Position <" << areaLights[i].vertices[j].x << ", " << areaLights[i].vertices[j].y << ", " << areaLights[i].vertices[j].z << ">" << endl;
		}
	}
}

void Scene::PrintSpheres() {
	for (int i = 0; i < sphereCount; i++) {
		printf("Sphere #%d\n", i);
		printf(" - Radius: %f\n", sph[i]->radius);
		/*cout << "Sphere #" << i << endl;
		cout << " - Radius: " << spheres[i].radius << endl;
		cout << " - Position: <" << spheres[i].position.x << ", " << spheres[i].position.y << ", " << spheres[i].position.z << ">" << endl;
		cout << " - Material: " << spheres[i].material << endl;
		cout << " - Refraction: " << spheres[i].refraction << endl;
		cout << " - Phong:" << endl;
		cout << "   - Diffuse: {" << spheres[i].color.diffuse.r << ", " << spheres[i].color.diffuse.g << ", " << spheres[i].color.diffuse.b << "}" << endl;
		cout << "   - Specular: {" << spheres[i].color.specular.r << ", " << spheres[i].color.specular.g << ", " << spheres[i].color.specular.b << "}" << endl;
		cout << "   - Shininess: " << spheres[i].color.shininess << endl;*/
	}
}

void Scene::PrintTriangles() {
	for (int i = 0; i < triangleCount; i++) {
		/*cout << "Triangle #" << i << endl;
		cout << " - Material: " << triangles[i].material << endl;
		cout << " - Vertices" << endl;
		for (int j = 0; j < 3; j++) {
			cout << "   - Vertex #" << j << endl;
			cout << "     - Position: <" << triangles[i].vertices[j].position.x << ", " << triangles[i].vertices[j].position.y << ", " << triangles[i].vertices[j].position.z << ">" << endl;
			cout << "     - Normal: <" << triangles[i].vertices[j].normal.x << ", " << triangles[i].vertices[j].normal.y << ", " << triangles[i].vertices[j].normal.z << ">" << endl;
			cout << "     - Phong:" << endl;
			cout << "       - Diffuse: {" << triangles[i].vertices[j].color.diffuse.r << ", " << triangles[i].vertices[j].color.diffuse.g << ", " << triangles[i].vertices[j].color.diffuse.b << "}" << endl;
			cout << "       - Specular: {" << triangles[i].vertices[j].color.specular.r << ", " << triangles[i].vertices[j].color.specular.g << ", " << triangles[i].vertices[j].color.specular.b << "}" << endl;
			cout << "       - Shininess: " << triangles[i].vertices[j].color.shininess << endl;
			cout << "     - UV: <" << triangles[i].vertices[j].texture_coord.u << ", " << triangles[i].vertices[j].texture_coord.v << ">" << endl;
			cout << "     - Refraction: " << triangles[i].vertices[j].refraction << endl;
		}*/
	}
}

void Scene::PrintPointLights() {
	for (int i = 0; i < pointLightCount; i++) {
		/*cout << "Point-Light #" << i << endl;
		cout << " - Position: <" << pointLights[i].position.x << ", " << pointLights[i].position.y << ", " << pointLights[i].position.z << ">" << endl;
		cout << " - Color: {" << pointLights[i].color.r << ", " << pointLights[i].color.g << ", " << pointLights[i].color.b << "}" << endl;*/
	}
}