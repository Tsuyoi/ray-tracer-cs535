#include "includes.h"

using namespace std;

TGAImage::TGAImage() {

}

TGAImage::TGAImage(int Width, int Height) {
	width = Width;
	height = Height;
	pixels = new ImageColor[width*height];
}

void TGAImage::setAllPixels(ImageColor * Pixels) {
	pixels = Pixels;
}

void TGAImage::setPixel(ImageColor inputColor, int x, int y) {
	pixels[convert2Dto1D(x, y)] = inputColor;
}

int TGAImage::convert2Dto1D(int x, int y) {
	return width * y + x;
}

void TGAImage::writeImage(string fileName) {
	if (width <= 0 || height <= 0) {
		cout << "Image size is not set properly" << endl;
		return;
	}
	ofstream o(fileName.c_str(), ios::out | ios::binary);
	o.put(0);							/* Write the Header */
	o.put(0);
	o.put(2);							/* Uncompressed RGB */
	o.put(0);	o.put(0);
	o.put(0);	o.put(0);
	o.put(0);
	o.put(0);	o.put(0);				/* X Origin */
	o.put(0);	o.put(0);				/* Y Origin */
	o.put((width & 0x00FF));
	o.put((width & 0xFF00) / 256);
	o.put((height & 0x00FF));
	o.put((height & 0xFF00) / 256);
	o.put(32);							/* 24 Bit Bitmap */
	o.put(0);
	// Write the Pixel Data
	for (int i = 0; i < width * height; i++) {
		o.put(pixels[i].b);
		o.put(pixels[i].g);
		o.put(pixels[i].r);
		o.put(pixels[i].a);
	}
	o.close();
}