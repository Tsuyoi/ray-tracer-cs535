#include "includes.h"

bool SphereHit(Ray r, Sphere * s, float &t) {
	Vector3f OC;
	OC = s->position - r.origin;
	float dist = sqrt(pow(OC.x, 2) + pow(OC.y, 2) + pow(OC.z, 2));
	float t_ca = OC * r.direction;
	if (t_ca < 0 && dist < s->radius)
		return false;
	float t_hc_2 = pow(s->radius, 2) - pow(dist, 2) + pow(t_ca, 2);
	if (t_hc_2 < 0)
		return false;
	float t_0 = 0;
	if (dist < s->radius)
		t_0 = t_ca + sqrt(t_hc_2);
	else
		t_0 = t_ca - sqrt(t_hc_2);
	if (t_0 < t && t_0 > 0.1f) {
		t = t_0;
		return true;
	} else
		return false;
}

bool TriangleHit(Ray r, Triangle * tri, float &t, float &u, float &v) {
	Vector3f e_1 = tri->vertices[1].position - tri->vertices[0].position;
	Vector3f e_2 = tri->vertices[2].position - tri->vertices[0].position;
	Vector3f pvec;
	pvec.x = r.direction.y * e_2.z - r.direction.z * e_2.y;
	pvec.y = r.direction.z * e_2.x - r.direction.x * e_2.z;
	pvec.z = r.direction.x * e_2.y - r.direction.y * e_2.x;
	float d = pvec * e_1;
	if (d == 0.0)
		return false;
	float inv_d = 1.0 / d;
	Vector3f tvec = r.origin - tri->vertices[0].position;
	float u_0 = (pvec * tvec) * inv_d;
	if (u_0 < 0.0 || u_0 > 1.0)
		return false;
	Vector3f qvec;
	qvec.x = tvec.y * e_1.z - tvec.z * e_1.y;
	qvec.y = tvec.z * e_1.x - tvec.x * e_1.z;
	qvec.z = tvec.x * e_1.y - tvec.y * e_1.x;
	float v_0 = (r.direction * qvec) * inv_d;
	if (v_0 < 0.0 || u_0 + v_0 > 1.0)
		return false;
	float t_0 = (e_2 * qvec) * inv_d;
	if (t_0 < t && t_0 > 0.1f) {
		t = t_0;
		u = u_0;
		v = v_0;
		return true;
	} else
		return false;
}

SimpleColor GetTexel(cimg_library::CImg<short> * img, float a_U, float a_V) {
	float fu = (a_U + 1000.0f) * img->width();
	float fv = (a_V + 1000.0f) * img->height();
	int u1 = ((int)fu) % img->width();
	int v1 = ((int)fv) % img->height();
	int u2 = (u1 + 1) % img->width();
	int v2 = (v1 + 1) % img->height();
	float fracu = fu - floor(fu);
	float fracv = fv - floor(fv);
	float w1 = (1 - fracu) * (1 - fracv);
	float w2 = fracu * (1 - fracv);
	float w3 = (1 - fracu) * fracv;
	float w4 = fracu * fracv;
	SimpleColor c1;
	c1.r = (float)img->atX(u1, v1, 0) / 255.0;
	c1.g = (float)img->atX(u1, v1, 1) / 255.0;
	c1.b = (float)img->atX(u1, v1, 2) / 255.0;
	SimpleColor c2;
	c2.r = (float)img->atX(u2, v1, 0) / 255.0;
	c2.g = (float)img->atX(u2, v1, 1) / 255.0;
	c2.b = (float)img->atX(u2, v1, 2) / 255.0;
	SimpleColor c3;
	c3.r = (float)img->atX(u1, v2, 0) / 255.0;
	c3.g = (float)img->atX(u1, v2, 1) / 255.0;
	c3.b = (float)img->atX(u1, v2, 2) / 255.0;
	SimpleColor c4;
	c4.r = (float)img->atX(u2, v2, 0) / 255.0;
	c4.g = (float)img->atX(u2, v2, 1) / 255.0;
	c4.b = (float)img->atX(u2, v2, 2) / 255.0;
	return (c1 * w1) + (c2 * w2) + (c3 * w3) + (c4 * w4);
}

SimpleColor RayTrace(Ray r, Scene * s, SpaceCoordinate cam, float coef, int iter) {
	SimpleColor ret;
	ret.r = 0.0;
	ret.g = 0.0;
	ret.b = 0.0;
	if (iter > 8)
		return ret;
	float t = 2000.0;
	Sphere * sph = NULL;
	Triangle * tri = NULL;
	float u = 0.0;
	float v = 0.0;
	float temp = 0.0;
	for (int i = 0; i < s->GetTriangleCount(); i++) {
		Triangle * tmp = s->GetTriangle(i);
		if (TriangleHit(r,tmp,t,u,v)) {
			tri = tmp;
		}
	}
	for (int i = 0; i < s->GetSphereCount(); i++) {
		Sphere * tmp = s->GetSphere(i);
		if (SphereHit(r,tmp,t)) {
			tri = NULL;
			sph = tmp;
		}
	}
	SpaceCoordinate intersect = r.origin + t * r.direction;
	if (intersect.z > cam.z)
		return ret;
	if (sph != NULL) {
		Vector3f norm = intersect - sph->position;
		norm.x /= sph->radius;
		norm.y /= sph->radius;
		norm.z /= sph->radius;
		if (sph->material == 0) { /* Phong */
			for (int i = 0; i < s->GetPointLightCount(); i++) {
				PointLight * pl = s->GetPointLight(i);
				Ray lRay;
				lRay.origin = intersect;
				lRay.direction = pl->position - intersect;
				if (norm * lRay.direction < 0.0f)
					continue;
				temp = lRay.direction * lRay.direction;
				if (temp == 0.0f)
					continue;
				temp = 1.0f / sqrt(temp);
				lRay.direction = temp * lRay.direction;
				temp = 2000.0;
				bool shadowed = false;
				for (int j = 0; j < s->GetSphereCount(); j++) {
					Sphere * tmp = s->GetSphere(j);
					if (SphereHit(lRay, tmp, temp)) {
						shadowed = true;
						break;
					}
				}
				temp = 2000.0;
				float tempU = 0;
				float tempV = 0;
				for (int j = 0; j < s->GetTriangleCount(); j++) {
					Triangle * tmp = s->GetTriangle(j);
					if (TriangleHit(lRay, tmp, temp, tempU, tempV)) {
						shadowed = true;
						break;
					}
				}
				if (!shadowed) {
					float diffuse = (lRay.direction * norm) * coef;
					if (diffuse < 0.0f)
						diffuse = 0.0f;
					ret.r += pl->color.r * sph->color.diffuse.r * diffuse;
					ret.g += pl->color.g * sph->color.diffuse.g * diffuse;
					ret.b += pl->color.b * sph->color.diffuse.b * diffuse;
					float ref = 2.0f * (lRay.direction * norm);
					Vector3f reflect = lRay.direction - ref * norm;
					float specular = r.direction * reflect;
					if (specular < 0.0f)
						specular = 0.0f;
					specular = pow(specular, 32) * coef;
					ret.r += pl->color.r * sph->color.specular.r * specular;
					ret.g += pl->color.g * sph->color.specular.g * specular;
					ret.b += pl->color.b * sph->color.specular.b * specular;
				}
			}
			Ray ref;
			ref.direction = r.direction - 2.0f * (r.direction * norm) * norm;
			ref.origin = intersect;
			SimpleColor reflected = RayTrace(ref, s, cam, coef * sph->refraction, iter + 1);
			ret = ret + reflected;
			ret.r += s->GetAmbientLight().color.r * sph->color.diffuse.r * coef;
			ret.g += s->GetAmbientLight().color.g * sph->color.diffuse.g * coef;
			ret.b += s->GetAmbientLight().color.b * sph->color.diffuse.b * coef;
		}
		if (sph->material == 2) { /* Mirror */
			Ray ref;
			ref.direction = r.direction - 2.0f * (r.direction * norm) * norm;
			ref.origin = intersect;
			SimpleColor reflected = RayTrace(ref, s, cam, coef, iter + 1);
			ret = ret + reflected;
		}
	}
	if (tri != NULL) {
		Vector3f norm = tri->vertices[0].normal + u * (tri->vertices[1].normal - tri->vertices[0].normal) + v * (tri->vertices[2].normal - tri->vertices[0].normal);
		float temp = norm * norm;
		if (temp == 0.0f)
			return ret;
		temp = 1.0f / sqrt(temp);
		norm = temp * norm;
		if (tri->material == 0) { /* Phong */
			SimpleColor texCol;
			float u1 = tri->vertices[0].texture_coord.u;
			float u2 = tri->vertices[1].texture_coord.u;
			float u3 = tri->vertices[2].texture_coord.u;
			float v1 = tri->vertices[0].texture_coord.v;
			float v2 = tri->vertices[1].texture_coord.v;
			float v3 = tri->vertices[2].texture_coord.v;
			float texU = u1 + u * (u2 - u1) + v * (u3 - u1);
			float texV = v1 + u * (v2 - v1) + v * (v3 - v1);
			texCol = GetTexel(tri->texture, texU, texV);
			SimpleColor diffuseColor = tri->vertices[0].color.diffuse + tri->vertices[1].color.diffuse + tri->vertices[2].color.diffuse;
			diffuseColor.r /= 3.0f;
			diffuseColor.g /= 3.0f;
			diffuseColor.b /= 3.0f;
			SimpleColor specularColor = tri->vertices[0].color.specular + tri->vertices[1].color.specular + tri->vertices[2].color.specular;
			specularColor.r /= 3.0f;
			specularColor.g /= 3.0f;
			specularColor.b /= 3.0f;
			for (int i = 0; i < s->GetPointLightCount(); i++) {
				PointLight * pl = s->GetPointLight(i);
				Ray lRay;
				lRay.origin = intersect;
				lRay.direction = pl->position - intersect;
				if (norm * lRay.direction <= 0.0f)
					continue;
				temp = lRay.direction * lRay.direction;
				if (temp == 0.0f)
					continue;
				temp = 1.0f / sqrt(temp);
				lRay.direction = temp * lRay.direction;
				temp = 2000.0;
				bool shadowed = false;
				for (int j = 0; j < s->GetSphereCount(); j++) {
					Sphere * tmp = s->GetSphere(j);
					if (SphereHit(lRay, tmp, temp)) {
							shadowed = true;
							break;
					}
				}
				temp = 2000.0;
				float tempU = 0;
				float tempV = 0;
				for (int j = 0; j < s->GetTriangleCount(); j++) {
					Triangle * tmp = s->GetTriangle(j);
					if (TriangleHit(lRay, tmp, temp, tempU, tempV)) {
						shadowed = true;
						break;
					}
				}
				if (!shadowed) {
					float diffuse = (lRay.direction * norm) * coef;
					ret.r += pl->color.r * diffuseColor.r * texCol.r * diffuse;
					ret.g += pl->color.g * diffuseColor.g * texCol.g * diffuse;
					ret.b += pl->color.b * diffuseColor.b * texCol.b * diffuse;
					if (diffuse < 0.0f)
						diffuse = 0.0f;
					float ref = 2.0f * (lRay.direction * norm);
					Vector3f reflect = lRay.direction - ref * norm;
					float specular = r.direction * reflect;
					if (specular < 0.0f)
						specular = 0.0f;
					specular = pow(specular, 32) * coef;
					ret.r += pl->color.r * specularColor.r * texCol.r * specular;
					ret.g += pl->color.g * specularColor.g * texCol.g * specular;
					ret.b += pl->color.b * specularColor.b * texCol.b * specular;
				}
			}
			float reflectionIndex = (tri->vertices[0].refraction + tri->vertices[0].refraction + tri->vertices[0].refraction) / 3.0f;
			Ray ref;
			ref.direction = r.direction - 2.0f * (r.direction * norm) * norm;
			ref.origin = intersect;
			SimpleColor reflected = RayTrace(ref, s, cam, coef * reflectionIndex, iter + 1);
			ret = ret + reflected;
			ret.r += s->GetAmbientLight().color.r * diffuseColor.r * texCol.r * coef;
			ret.g += s->GetAmbientLight().color.g * diffuseColor.g * texCol.g * coef;
			ret.b += s->GetAmbientLight().color.b * diffuseColor.b * texCol.b * coef;
		}
		if (tri->material == 2) { /* Mirror */
			Ray ref;
			ref.direction = r.direction - 2.0f * (r.direction * norm) * norm;
			ref.origin = intersect;
			SimpleColor reflected = RayTrace(ref, s, cam, coef, iter + 1);
			ret = ret + reflected;
		}
	}
	return ret;
}

int main(int argc, char **argv) {
	printf("Loading Scene...\n");
	Scene * scene = new Scene();
	scene->LoadScene("sample-scene.txt");
	SpaceCoordinate Camera;
	Camera.x = Camera.y = 0.0;
	Camera.z = 22.0;
	float bound = 10.0;
	int width = 1600;
	int height = 1200;
	TGAImage * image = new TGAImage(width, height);
	ImageColor c;
	SimpleColor color;
	c.a = 255.0f;
	printf("Generating Image...\n");
	for (int x = 0; x < width; x++) {
		for (int y = 0; y < height; y++) {
			c.r = 0;
			c.g = 0;
			c.b = 0;
			color.r = 0;
			color.g = 0;
			color.b = 0;
			float coef = 0.25f;
			for (float fragX = (float)x; fragX < (float)x + 1.0f; fragX += 0.5f)
				for (float fragY = (float)y; fragY < (float)y + 1.0f; fragY  += 0.5f) { /* Anti-Aliasing */
					Ray ray;
					ray.origin.x = ((float)fragX - ((float)width / 2.0)) / (float)(width+height)/2.0 * 20.0;
					ray.origin.y = ((float)fragY - ((float)height / 2.0)) / (float)(width+height)/2.0 * 20.0;
					ray.origin.z = bound + 7.0;
					ray.direction = ray.origin - Camera;
					float temp = ray.direction * ray.direction;
					if (temp == 0.0f)
						break;
					temp = 1.0f / sqrt(temp);
					ray.direction = temp * ray.direction;
					color = color + RayTrace(ray, scene, Camera, coef, 0);
				} /* Anti-Aliasing */
			c.r = min(color.r * 255.0f, 255.0f);
			c.g = min(color.g * 255.0f, 255.0f);
			c.b = min(color.b * 255.0f, 255.0f);
			image->setPixel(c, x, y);
		}
	}
	delete scene;
	printf("Saving Image...\n");
	string path = "test.tga";
	image->writeImage(path);
	return 0;
}